---
layout: post
title:  "the openwire escapade, part 1"
date:   2018-4-23
categories: rust networking
---

documenting my progress of building [openwire](https://github.com/hello-marmalade/openwire)

(part 1)

i decided to search for any existing open source projects that had already done what i wanted to do. (edit 2018.4.24)[there was,] sort of, but with no ui.

i eventually found [netinfo](https://github.com/kaegi/netinfo), which was made in rust, and uses [libpnet](https://github.com/libpnet/libpnet) - a networking library similar to libpcap, but as with netinfo, entirely written in rust..

perfect.

initially, there was an issue with the versions of libpnet and netinfo that caused netinfo to not build correctly, but about four days ago the developer of netinfo updated it.

perfect again.

so now what to do?

well the project is essentially to create a piece of software similar to the proprietary windows project glasswire. a firewall that can monitor and block traffic on a per application basis. for my project i'll be focusing on monitoring first, and then integrate firewall rules by using some kind of iptables library. 

SO - uhhh... where was i

oh right. probably gonna be using gtk for the ui library... and uh... i dunno the design's gonna be pretty simple. kinda like gnome's system monitor, but for networking information on each of the processes. so now i need to figure out pulling the data i need with netinfo, and piping it into gtk structures.

so that's what's up now. i guess.

until next time.