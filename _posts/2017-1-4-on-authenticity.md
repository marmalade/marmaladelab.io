---
layout: post
title: "on authenticity"
date: 2017-1-4
categories: thoughts
---

authenticity seems to be in short supply these days. i think that's why right now we're at what appears to be an all time low in trusting. nobody trusts anything anymore. or well they do, in a way, but they mostly seem to trust their distrust in everyone and everything else.

my working theory is that it's because we're constantly propagandized all day by once again, everyone, and everything. hell it used to be that the only things really propagandizing you were advertisements. print media, television... i mean you could argue that our news sources have always been doing that, but what i mean to say that is before, what was propagandizing you came in the form of outside media, but now, with the advent of social media, everybody's propagandizing everyone. who you are has become a form of advertisement, now. everything is a lie, a cleverly disguised campaign. what can you trust? nothing out there feels genuine anymore, because everything has to be amazing. extreme. everything is fighting for your attention, and it's doing it with 5 EXTREME ADJECTIVES THAT YOU NEED TO SEE. but when everything is maximally important, it all becomes background noise.

to me, lately the things that stand out the most are the understated things. the subtle things. the things that don't demand attention, or even ask for it. it's a relief.

i think we all long for something a little more genuine these days.

or at least i hope we do.
