---
layout: post
title:  "running observium on docker"
date:   2016-11-30
categories: docker observium
---

right so this took me probably longer than it should have but i figured out how to use yelp's observium docker container https://github.com/Yelp/docker-observium

it's not a fire and forget, it requires a little bit of... prep and configuration which is not information that's immediately obvious if you're new to this kind of thing. basically, unless you wanna go in and mod the dockerfile you need to have a mysql/mariadb server up and running somewhere (or postgresql i'm p sure observium supports it) and then put that information into the config.php. you can run the yelp container once using their instructions to get the files generated and put where they need to be, and then you need to mod the config.php so that it has the mysql login info. on the mysql server you need to follow the sql setup guide http://www.observium.org/docs/install_debian/#mysql-database and then once that's setup you need to hop into the observium container and run your ./adduser.php to make your admin user, or whatever user you're making. http://www.observium.org/docs/install_debian/#add-initial-user-and-device

maybe i'll make a little guide for setting this up later.

note: i may just keep saying this and not doing it, though.